//
//  ViewController.swift
//  Lab02
//
//  Created by Eungyo on 1/23/17.
//  Copyright © 2017 Eungyo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField01: UITextField!
    
    
    @IBOutlet weak var myLabel01: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func myButton(_ sender: Any) {
        //variable name is anything that input into textfield area
        let name = textField01.text!
        //pass the data from textfield into label
        myLabel01.text = "Hi, my name is \(name)!"
        //hide the keyboard after tapping the button
        textField01.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

